package pl.sda.tdd.sklep;

public class KartaKlienta {

    double valueSpent = 0;

    public int pobierzLiczbeKuponow() {
        if(valueSpent < 0) throw new IllegalArgumentException();
        else if(valueSpent < 40.00d) return 0;
        else if(valueSpent < 80.00d * 100/95) return 1;
        else if(valueSpent < 120.00d * 100/90) return 2;
        else if(valueSpent < 160.00d * 100/90) return 3;
        else if(valueSpent < 200.00d * 100/90) return 4;
        else return 4;
    }

    public void zrobZakup(double amount) {
        valueSpent += amount;
    }

    public static void main(String[] args) {
        System.out.println(80.00d * 100/95);
        System.out.println(80.00d * 1.05);
    }
}
