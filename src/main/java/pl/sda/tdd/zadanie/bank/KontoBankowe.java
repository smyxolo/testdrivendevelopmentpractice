package pl.sda.tdd.zadanie.bank;

public class KontoBankowe {
    private String wlasciciel;
    private int stanKonta;
    private int limitDzienny;
    private int kodPin;

    public KontoBankowe(String wlasciciel, int stanKonta, int limitDzienny, int kodPin) {
        this.wlasciciel = wlasciciel;
        this.stanKonta = stanKonta;
        this.limitDzienny = limitDzienny;
        this.kodPin = kodPin;
    }

    public boolean przelejKwote(Bank bank, KontoBankowe kontoBankowe, int kwota, int kodPin) {
        if(kodPin == this.kodPin && kwota <= limitDzienny && kwota <= stanKonta) {
            stanKonta -= kwota;
            kontoBankowe.wplacSrodki(kwota);
            updateRejestr(bank, wlasciciel, kontoBankowe, kwota, true);
            return true;
        }
        else {
            updateRejestr(bank, wlasciciel, kontoBankowe, kwota, false);
            return false;
        }
    }

    private void updateRejestr(Bank bank, String nadawca, KontoBankowe kontoOdbiorcy, Integer kwota, boolean success){
        StringBuilder stbd = new StringBuilder();
        stbd.append(nadawca);
        stbd.append(" do ");
        stbd.append(kontoOdbiorcy.getWlasciciel());
        stbd.append(" => przelew ");
        stbd.append(" PLN, ");
        stbd.append(success);
        stbd.append(".");
        bank.getRejestr().add(stbd.toString());
    }

    private void wplacSrodki(int kwota) {
        this.stanKonta += kwota;
    }

    public int pobierzStanKonta() {
        return stanKonta;
    }

    public String getWlasciciel() {
        return wlasciciel;
    }
}
