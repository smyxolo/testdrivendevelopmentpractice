package pl.sda.tdd.spec;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class WiekOsoby {

    /**
     * oblicza wiek osoby w danym dniu
     * @param dataUrodzenia - wymagany foramat dd-MM-YYYY, np. "01-11-2000"
     * @param wybranaData - wymagany foramat dd-MM-YYYY, np. "01-11-2000"
     * @return wiek osoby obliczony dla danego dnia
     * wymaga dataUrodzenia <= wybranaData
     * @throws IllegalArgumentException
     */

    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static int obliczWiek(String dataUrodzenia, String wybranaData){
        if(!parseDate(dataUrodzenia).isBefore(parseDate(wybranaData))) throw new IllegalArgumentException("Nieprawidlowy dobor dat");
        else return Period.between(parseDate(dataUrodzenia), parseDate(wybranaData)).getYears();
    }

    private static LocalDate parseDate(String data){
        return LocalDate.parse(data, dtf);
    }

    public static void main(String[] args){
        System.out.println(WiekOsoby.obliczWiek("03-02-2009", "01-01-2000"));
    }
}
