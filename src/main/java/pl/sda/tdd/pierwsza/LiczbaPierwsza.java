package pl.sda.tdd.pierwsza;

public class LiczbaPierwsza {
    public static boolean czyLiczbaPierwszaP(int liczba) {
        for (int i = 2; i < liczba; i++) {
            if(liczba % i == 0) return false;
        }
        return true;
    }

    public static boolean czyLiczbaPierwsza(int liczba) {
        if(liczba <= 1) return false;
        for (int i = 2; i <= liczba/2; i++) {
            if(liczba % i == 0) return false;
        }
        return true;
    }
}
