package pl.sda.tdd.test.zadanie.bank;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.sda.tdd.zadanie.bank.Bank;
import pl.sda.tdd.zadanie.bank.KontoBankowe;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.Matchers.*;

public class RejestrTest {
    Bank bank = new Bank();
    KontoBankowe k1 = new KontoBankowe("Tomasz",2000, 1000, 1234);
    KontoBankowe k2 = new KontoBankowe("Marek",0, 800, 1231);

    @Before
    public void zresetujKonta(){
        k1 = new KontoBankowe("Tomasz",2000, 1000, 1234);
        k2 = new KontoBankowe("Marek",0, 800, 1231);

    }

    @Test
    public void naStarcieRejestrTransakcjiJestPusty(){
        Assert.assertThat(bank.getRejestr(), empty());
    }

//    @Test
//    public void poNieudanejProbiePrzelewuRejestrDalejPusty(){
//        k1.przelejKwote(bank, k2, 100, 1233);
//        Assert.assertThat(bank.getRejestr(), empty());
//    }

    @Test
    public void poNieudanejProbiePrzelewuZapisZawieraFalse(){
        k1.przelejKwote(bank, k2, 100, 1233);
        Assert.assertThat(bank.getRejestr().get(0), both(containsString("Tomasz"))
                .and(containsString("Marek")).and(not(containsString("true"))));
    }

    @Test
    public void transakcjaZawieraInformacjeONadawcyIOdbiorcy(){
        k1.przelejKwote(bank, k2, 100, 1234);
        Assert.assertThat( bank.getRejestr().get(0), both(containsString("Tomasz")).and(containsString("Marek")));
    }

}
