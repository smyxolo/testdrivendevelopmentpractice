package pl.sda.tdd.test.zadanie.bank;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.sda.tdd.zadanie.bank.Bank;
import pl.sda.tdd.zadanie.bank.KontoBankowe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrzelewTest {
    KontoBankowe kontoAdama;
    KontoBankowe kontoEwy;
    Bank bank;

    @Before
    public void przygotujKonta(){
        bank = new Bank();
        kontoAdama = new KontoBankowe("Adam", 0, 1000, 1234);
        kontoEwy = new KontoBankowe("Ewa",10000, 1000, 5555);
    }

    @Test
    public void przelejKwoteDlaSpelnionychWarunkow(){
        assertTrue(kontoEwy.przelejKwote(bank, kontoAdama, 500, 5555));
    }

    @Test
    public void przelewNaKontoZwiekszaStanKonta(){
       kontoEwy.przelejKwote(bank, kontoAdama, 500, 5555);
        assertEquals(500, kontoAdama.pobierzStanKonta());
    }

    @Test
    public void niePrzelewajDlaZlegoPinu(){
        assertFalse(kontoEwy.przelejKwote(bank, kontoAdama, 124, 7878));
    }
}