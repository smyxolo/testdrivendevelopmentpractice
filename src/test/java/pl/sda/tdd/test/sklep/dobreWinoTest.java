package pl.sda.tdd.test.sklep;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.sklep.KartaKlienta;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class dobreWinoTest {
    KartaKlienta karta;

    @Before

    public void utworzKarte(){
        karta = new KartaKlienta();
    }

    @Test
    public void zakupPonizej40Daje0Bonow(){
        karta.zrobZakup(39.99d);
        assertEquals(0, karta.pobierzLiczbeKuponow());
    }

    @Test
    public void zakup40Daje1Bon(){
        karta.zrobZakup(40.00d);
        assertEquals(1, karta.pobierzLiczbeKuponow());
    }

}
