package pl.sda.tdd.test.sklep;

        import org.junit.Before;
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.junit.runners.Parameterized;
        import pl.sda.tdd.sklep.KartaKlienta;

        import java.util.Arrays;
        import java.util.Collection;

        import static org.junit.Assert.assertEquals;


@RunWith(Parameterized.class)
public class DobreWinoParamTest {
    public KartaKlienta karta;
    public double kwota;
    public int liczbaKuponow;

    public DobreWinoParamTest(double kwota, int liczbaKuponow){
        this.kwota = kwota;
        this.liczbaKuponow = liczbaKuponow;
    }

    @Parameterized.Parameters (name = "{index} : dla {0} wychoddzi {1} ?")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {39.99d, 0},
                {40.00d, 1},
//                {79.99d, 1},
//                {80.00d, 2},
//                {119.99d, 2},
//                {120.00d, 3},
                {84.20d, 1},
                {84.22d, 2},
                {133.32d, 2},
                {133.34d, 3}});
    }

    @Before
    public void utworzKarte(){
        karta = new KartaKlienta();
    }

    @Test
    public void zrobZakupTest(){
        karta.zrobZakup(kwota);
        assertEquals(liczbaKuponow, karta.pobierzLiczbeKuponow());
    }


}
