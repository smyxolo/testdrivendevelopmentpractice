package pl.sda.tdd.test.pierwsza;

import org.junit.Test;
import pl.sda.tdd.pierwsza.LiczbaPierwsza;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LiczbaPierwszaTest {

    @Test
    public void siedemJestLiczbaPierwsza(){
        assertTrue(LiczbaPierwsza.czyLiczbaPierwsza(7));
    }

    @Test
    public void osiemNieJestLiczbaPierwsza(){
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(8));
    }

    @Test
    public void dziewiecNieJestLiczbaPierwsza(){
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(9));
    }

    @Test
    public void jedenNieJestLiczbaPierwsza(){
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(1));
    }

    @Test
    public void liczbaUjemnaNieJestLiczbaPierwsza(){
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(-91));
    }
}
