package pl.sda.tdd.test.pierwsza;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.pierwsza.LiczbaPierwsza;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class LiczbaPierwszaTestParametryzowany {

    @Parameterized.Parameter(value = 0)
    public int liczba;

    @Parameterized.Parameter(value = 1)
    public boolean czyPierwsza;

    @Parameterized.Parameters (name = "{index} : czy liczba {0} powinna byc liczba pierwsza? {1}")
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][]{
                {1, false},
                {7, true},
                {8, false},
                {4, false},
                {9, false}});
    }

    @Test
    public void czyLiczbaPierwszaTest(){
        assertEquals(czyPierwsza, LiczbaPierwsza.czyLiczbaPierwsza(liczba));
    }
}
