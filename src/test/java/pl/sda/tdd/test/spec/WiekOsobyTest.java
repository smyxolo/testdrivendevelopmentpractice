package pl.sda.tdd.test.spec;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.spec.WiekOsoby;

import java.time.Period;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RunWith(Parameterized.class)
public class WiekOsobyTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Parameterized.Parameter(value = 0)
    public String dataUrodzenia;

    @Parameterized.Parameter(value = 1)
    public String dataDoSprawdzenia;

    @Parameterized.Parameter(value = 2)
    public int wiekDlaDaty;


    @Parameterized.Parameters(name = "{index} : start {0} / koniec {1}: daje {2} lat.")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {"01-01-2000", "01-05-2005", 5},
                {"01-01-1960", "01-05-1965", 5},
                {"01-01-2002", "31-01-2011", 9},
                {"01-01-1992", "31-01-2099", 107},
                {"02-01-2001", "01-01-2005", 3}});
    }

    @Test
    public void dataUrodzeniaWiekszaOdWybranegoDniaZwracaWyjatek(){
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Nieprawidlowy dobor dat");
        WiekOsoby.obliczWiek(dataDoSprawdzenia, dataUrodzenia);
    }

    @Test
    public void podaneDatyZwracajaOdpowiedniWiek(){
        Assert.assertEquals(wiekDlaDaty, WiekOsoby.obliczWiek(dataUrodzenia, dataDoSprawdzenia));
    }

}
